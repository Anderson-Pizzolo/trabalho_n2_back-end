# URL Shortener Service

Este é um serviço simples de encurtamento de URLs desenvolvido em Java com Spring Boot.

## Visão Geral

O serviço permite que os usuários convertam URLs longas em URLs curtas para facilitar o compartilhamento. Ele usa um serviço de encurtamento de URL externo para gerar as URLs curtas.

## Funcionalidades

- Encurtamento de URLs longas para URLs curtas.
- Redirecionamento de URLs curtas para suas versões originais.


## Tecnologias Utilizadas

- Java
- Spring Boot
- Maven



### Encurtar uma URL

Faça uma requisição POST para `/shorten` com o seguinte corpo em JSON:

```json
{
  "url": "URL_LONGA_A_SER_ENCURTADA"
}
