package Projeto.Backend.com.LinkSquish.services;

import Projeto.Backend.com.LinkSquish.modelo.Links;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class URLShortenerService {
    private final String API_BASE_URL = "https://csclub.uwaterloo.ca/~phthakka/1pt-express";
    private Map<String, String> urlMap = new HashMap<>();
    private RestClient client = RestClient.create(API_BASE_URL);

    private List<Links> listadelinks;
    @Autowired
    private RestTemplate restTemplate;

    public String shortenURL(String longURL) {
       String json = client.post().uri(uriBuilder ->
               uriBuilder.path("/addUrl")
                       .queryParam("long",longURL )
                       .build())
               .retrieve().body(String.class);

       System.out.println("aqui");
       System.out.println(json);
       return json;

    }

    public String getOriginalURL(String shortURL) {
        return urlMap.getOrDefault(shortURL, null);
    }
}



