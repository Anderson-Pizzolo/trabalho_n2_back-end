package Projeto.Backend.com.LinkSquish.controller;

import Projeto.Backend.com.LinkSquish.modelo.Links;
import Projeto.Backend.com.LinkSquish.services.URLShortenerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GeoLocationController {
    @Autowired
    private URLShortenerService urlShortenerService;

    @PostMapping("/shorten")
    public ResponseEntity<String> shortenURL(@RequestParam String links) {
        String shortURL = urlShortenerService.shortenURL(links);
        System.out.println(links);
        if (shortURL != null) {
            return ResponseEntity.ok(shortURL);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao encurtar a URL");
        }
    }

    @GetMapping("/shorten/{shortURL}")
    public ResponseEntity<String> redirectToOriginalURL(@PathVariable String shortURL) {
        String originalURL = urlShortenerService.getOriginalURL(shortURL);
        if (originalURL != null) {
            return ResponseEntity.status(HttpStatus.FOUND).header("Location", originalURL).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("URL não encontrada");
        }
    }


}
