package Projeto.Backend.com.LinkSquish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinkSquishApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinkSquishApplication.class, args);
	}

}
