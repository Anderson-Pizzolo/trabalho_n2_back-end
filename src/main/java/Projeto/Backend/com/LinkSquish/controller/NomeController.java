package Projeto.Backend.com.LinkSquish.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NomeController {
    @GetMapping("/ajuda")
    public String getAjuda() {
        return "{\n" +
                "    \"estudante\": \"Anderson Pizzolo\",\n" +
                "    \"projeto\": \"LinkSquish\"\n" +
                "}";
    }
}
