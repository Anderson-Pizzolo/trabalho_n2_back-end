package Projeto.Backend.com.LinkSquish.modelo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Links {

    private String message ;
    @JsonProperty ("short")
    private String url_short ;
    @JsonProperty ("long")
    private String url_long ;

    public Links(String message, String url_short, String url_long) {
        this.message = message;
        this.url_short = url_short;
        this.url_long = url_long;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl_short() {
        return url_short;
    }

    public void setUrl_short(String url_short) {
        this.url_short = url_short;
    }

    public String getUrl_long() {
        return url_long;
    }

    public void setUrl_long(String url_long) {
        this.url_long = url_long;
    }
}
